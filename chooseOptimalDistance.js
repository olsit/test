const chooseOptimalDistance = (maxDistance, quantityPlaces, arrDistance) => {

  if (maxDistance < 0 || !Number.isInteger(maxDistance)) {
    throw new Error('The maximum distance must not be less than 0 and must be an integer');
  }

  if (quantityPlaces < 1 || !Number.isInteger(quantityPlaces)) {
    throw new Error('The number of places to visit must be greater than 0 and must be an integer');
  }

  if (arrDistance.length === 0 || !Array.isArray(arrDistance)) {
    throw new Error('The list of distances must not be empty and must be an array');
  }

  if (arrDistance.length < quantityPlaces) {
    return null;
  }

  const allCombinationsDistance = (arrDistance, quantityPlaces) => {
    if (quantityPlaces === 1) {
      return arrDistance.map(item => {
        if (item < 0 || !Number.isInteger(item)) {
          throw new Error('Each element of the list of distances must not be less than 0 and must be an integer');
        }

        return [item];
      });
    }

    let result = [];

    for (let i = 0; i <= (arrDistance.length - quantityPlaces); i++) {
      if (arrDistance[i] < 0 || !Number.isInteger(arrDistance[i])) {
        throw new Error('Each element of the list of distances must not be less than 0 and must be an integer');
      }

      let element = allCombinationsDistance(arrDistance.slice(i + 1), quantityPlaces - 1);

      for (let j = 0; j < element.length; j++) {
        result.push(element[j].concat([arrDistance[i]]));
      }
    }

    return result;
  }

  let distances = allCombinationsDistance(arrDistance, quantityPlaces).map(item => {
    item = item.reduce((value, current) => value + current);

    if (item > maxDistance) {
      return null;
    }

    return item;
  });

  let optimalDistance = Math.max(...distances);

  if (optimalDistance) {
    return optimalDistance;
  }

  return null;
};
